"use strict";

/**
 * The ball to be used during block breaking phase.
 */
export class Ball {
    constructor(options = {}) {
        this._radius = options.radius || 0.125;
        var geometry = new THREE.SphereGeometry(this._radius, 32, 32);
        let material = new THREE.MeshStandardMaterial({
            color: 0xff00ff,
            roughness: 0.1,
        });
        this._mesh = new THREE.Mesh( geometry, material );
        this._direction = options.direction || -Math.PI / 4;
        this._velocity = options.velocity || 0.01;

        this._lastPosition = new THREE.Vector2(0, 0);
    }

    /**
     * Retrieves the vector related to this ball.
     */
    get vector() {
        return new THREE.Vector2(
          Math.cos(this._direction) * this._velocity,
          Math.sin(this._direction) * this._velocity
        );
    }

    /**
     * Updates the direction/velocity of the ball via the given vector.
     *
     * @param {THREE.Vector2} value The vector to use.
     */
    set vector(value) {
        this._velocity = value.length();
        this._direction = value.angle();
    }

    /**
     * Retrieves the current angle of the ball.
     */
    get direction() {
        return this._direction;
    }

    /**
     * Sets the direction the ball is facing.
     */
    set direction(value) {
        this._direction = value;
    }

    /**
     * Retrieves the current velocity of the ball.
     */
    get velocity() {
        return this._velocity;
    }

    /**
     * Sets the current velocity of the ball.
     */
    set velocity(value) {
        this._velocity = value;
    }

    /**
     * Retrieves the mesh object for the ball.
     */
    get mesh() {
        return this._mesh;
    }

    /**
     * Retrieves the current radius.
     */
    get radius() {
        return this._radius;
    }

    /**
     * Retrieves the current x position.
     */
    get x() {
        return this._mesh.position.x;
    }

    /**
     * Retrieves the current y position.
     */
    get y() {
        return this._mesh.position.y;
    }

    /**
     * Retrieves the current position as a vector.
     */
    get position() {
        return new THREE.Vector2(this.x, this.y);
    }
    
    /**
     * Retrieves the position prior to the last move.
     */
    get lastPosition() {
        return this._lastPosition;
    }

    /**
     * Moves the ball within the scene to the given x and y coordinate.
     */
    move(x, y) {
        this._lastPosition = this.position;

        this._mesh.position.x = x;
        this._mesh.position.y = y;
    }

    /**
     * Moves the ball in its current direction.
     */
    update(elapsed) {
        let x = this.x + Math.cos(this._direction) * this._velocity * elapsed;
        let y = this.y + Math.sin(this._direction) * this._velocity * elapsed;

        this.move(x, y);
    }

    /**
     * Returns whether or not the body intersects the given rectangle.
     */
    intersectsRectangle(x, y, w, h) {
        let x2 = x + w;
        let y2 = y + h;

        x -= this.radius;
        x2 += this.radius;
        y -= this.radius;
        y2 += this.radius;

        // If the center of the circle is within the rectangle
        return (this.x >= x && this.x <= x2 && this.y >= y && this.y <= y2);
    }
}
