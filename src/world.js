"use strict";

/**
 * Manages our game world.
 */
export class World {
    constructor() {
        this._scene = new THREE.Scene();
        this._camera = new THREE.PerspectiveCamera( 35, window.innerWidth / window.innerHeight, 0.1, 1000 );

        var light = new THREE.PointLight(0xffffff, 1, 100);
        light.position.set(0, 0, 50);
        this._scene.add(light);

        var light = new THREE.PointLight(0x888888, 1, 100);
        light.position.set(0, 0, 0);
        this._scene.add(light);

        this._camera.position.z = 35;
    }

    add(object) {
        this._scene.add(object);
    }

    get scene() {
        return this._scene;
    }

    get camera() {
        return this._camera;
    }
}
