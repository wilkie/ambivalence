"use strict";

import { Input } from "./input.js";
import { Game } from "./game.js";
import { World } from "./world.js";
import { Renderer } from "./renderer.js";

/**
 * The main game class.
 */
export class Ambivalence {
    constructor(element) {
        this._world = new World();
        this._game = new Game(this._world);
        this._input = new Input({
          bindings: Ambivalence.BINDINGS,
          onAction: this._game.perform.bind(this._game)
        });
        this._renderer = new Renderer(element, this._game, this._world);
    }

    get world() {
        return this._world;
    }
}

Ambivalence.BINDINGS = {
    "MoveLeft": ["ArrowLeft"],
    "MoveRight": ["ArrowRight"],
    "MoveDown": ["ArrowDown"],
    "RotateRight": ["ArrowUp", "KeyX"],
    "RotateLeft": ["KeyZ"],
    "Drop": ["Space"]
};

export default Ambivalence;
