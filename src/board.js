"use strict";

import { Cube } from "./cube.js";

/**
 * The game board where the action happens.
 */
export class Board {
    constructor() {
        // Construct the geometry of the game board
        this._width = 10;
        this._height = 20;

        // The board edges material
        let material = new THREE.MeshStandardMaterial({
            color: 0xffffff,
            roughness: 0.5,
            metalness: 1.0,
        });

        // Top edge material
        let topMaterial = new THREE.MeshStandardMaterial({
            color: 0x0,
            roughness: 0.5,
            metalness: 1.0,
        });

        topMaterial.visible = false;

        // A basic cube mesh
        var geometry = new THREE.BoxGeometry();

        // Cubes for each side
        this._bottom = new THREE.Mesh(geometry, material);
        this._bottom.userData = { type: "Bottom" };
        this._left = new THREE.Mesh(geometry, material);
        this._left.userData = { type: "Left" };
        this._right = new THREE.Mesh(geometry, material);
        this._right.userData = { type: "Right" };
        this._top = new THREE.Mesh(geometry, topMaterial);
        this._top.userData = { type: "Top" };

        // Move and scale each side
        this._bottom.scale.x = 10;
        this._bottom.scale.y = 0.5;
        this._bottom.position.y = -10;
        this._bottom.layers.enable(1);

        this._top.scale.x = 10;
        this._top.scale.y = 0.5;
        this._top.position.y = +10;
        this._top.layers.enable(1);

        this._left.scale.x = 0.5;
        this._left.scale.y = 20;
        this._left.position.x = -5.25;
        this._left.position.y = -0.25;
        this._left.layers.enable(1);

        this._right.scale.x = 0.5;
        this._right.scale.y = 20;
        this._right.position.x = 5.25;
        this._right.position.y = -0.25;
        this._right.layers.enable(1);

        // Add edges to the group
        this._scene = new THREE.Scene();
        this._scene.add(this._bottom);
        this._scene.add(this._top);
        this._scene.add(this._left);
        this._scene.add(this._right);

        // The blocks comprising the 'stack'
        this._blocks = Array.from({ length: 20 }, (_) => {
            return new Array(10);
        });
    }

    /**
     * Retrieves the board width in block units.
     */
    get width() {
        return this._width;
    }

    /**
     * Retrieves the board height in block units.
     */
    get height() {
        return this._height;
    }

    get piece() {
        return this._piece;
    }

    set piece(value) {
        // Remove existing piece
        if (this._piece) {
            this._scene.remove(this._piece.scene);
        }

        // Add the given piece
        this._piece = value;

        if (this._piece) {
            this._piece.scene.position.x = -1.5;
            this._piece.scene.position.y = 7.75;
            this._scene.add(this._piece.scene);

            // Move piece to top
            this._pieceY = 17;
            this._pieceX = 3;
        }
    }

    /**
     * Retrieves the current Ball in the game board, if any.
     */
    get ball() {
        return this._ball;
    }

    /**
     * Places the given Ball into the board.
     */
    set ball(value) {
        // Remove the current ball, if any.
        if (this._ball) {
            this._scene.remove(this._ball.mesh);
        }

        this._ball = value;

        // Position on piece
        this._ballX = this.pieceX;
        this._ballY = this.pieceY;

        // Add to scene
        this._scene.add(this._ball.mesh);
    }

    /**
     * Retrieves the given scene representing the game board.
     */
    get scene() {
        return this._scene;
    }

    /**
     * Moves the current piece along the X-axis the given number of blocks.
     */
    move(delta) {
        if (!this.piece) {
            return;
        }

        this.piece.scene.position.x += delta;
        this._pieceX += delta;
    }

    /**
     * Moves the ball to the given local coordinates.
     */
    moveBall(x, y) {
        // Give up if there is no ball on the field.
        if (!this._ball) {
            return;
        }

        //this._ball.mesh.position.y -= elapsed / 1000 * 4;
    }

    /**
     * Determines if the piece currently collides.
     *
     * Returns the position of the first collided cube.
     */
    pieceCollides() {
        let collision = 99;
        this.piece.cubes.forEach( (point) => {
            let x = point.x + this._pieceX;
            let y = point.y + this._pieceY;

            let index = (point.x + 1 + ((3 - point.y) * 3));

            if (x >= this._width) {
                collision = Math.min(collision, index);
            }
            else if (x < 0) {
                collision = Math.min(collision, index);
            }
            else if (y < 0) {
                collision = Math.min(collision, index);
            }
            else if (this._blocks[y] && this._blocks[y][x]) {
                collision = Math.min(collision, index);
            }
        });

        if (collision == 99) {
            collision = 0;
        }

        return collision;
    }

    /**
     * Determines if the ball currently collides and returns where.
     */
    ballCollides() {
        let ballX = this._ball.x + (this.width / 2);
        let ballY = this._ball.y + (this.height / 2) - 0.25;
        let radius = this._ball.radius;

        let vector = this.ball.vector;

        let distance = this.ball.position.distanceTo(this.ball.lastPosition);

        // Create a raycaster at the previous location of the ball.
        let position3d = new THREE.Vector3(
            this.ball.lastPosition.x,
            this.ball.lastPosition.y,
            0
        );

        let direction3d = (new THREE.Vector3(
            vector.x,
            vector.y,
            0
        )).normalize();

        // Add the radius in the direction, the point we care about is the
        // leading point of the ball
        position3d.add(direction3d.clone().multiplyScalar(this.ball.radius));

        let raycaster = new THREE.Raycaster(position3d, direction3d, 0, distance);
        raycaster.layers.set(1);

        let objects = raycaster.intersectObjects(this.scene.children, true);

        for (let i = 0; i < objects.length; i++) {
            let intersected = objects[i];
            let point = intersected.point;

            let type = intersected.object.userData.type;
            if (type == "Bottom") {
                // 'Top' wall (bottom edge)
                vector = vector.multiply(new THREE.Vector2(1, -1));
            }
            else if (type == "Top") {
                // 'Bottom' opening (top opening)
                vector = vector.multiply(new THREE.Vector2(1, -1));
            }
            else if (type == "Left") {
                // Left wall
                vector = vector.multiply(new THREE.Vector2(-1, 1));
            }
            else if (type == "Right") {
                // Right wall
                vector = vector.multiply(new THREE.Vector2(-1, 1));
            }
            else if (type == "Cube" &&
                intersected.object.userData.instance.boardX == -1) {
                // Current piece (a cube that is not on the board)
                let cube = intersected.object.userData.instance;

                let x1 = cube.x + this.piece.x - 0.5;
                let x2 = x1 + 1;
                let y1 = cube.y + this.piece.y - 0.5;
                let y2 = y1 + 1;

                let x = cube.x;
                let y = cube.y;

                let blocks = [[false, false, false, false],
                              [false, false, false, false],
                              [false, false, false, false],
                              [false, false, false, false]];

                this.piece.cubes.forEach( (block) => {
                    blocks[block.y][block.x] = true;
                });

                let above = (blocks[y + 1] || [])[x];
                let below = (blocks[y - 1] || [])[x];
                let toLeft = blocks[y][x - 1];
                let toRight = blocks[y][x + 1];

                // Determine where each of the cubes intersected with the ball.
                let found = false;

                if ((point.y == y1 && !below) || (point.y == y2 && !above)) {
                    // Top/Bottom
                    vector = vector.multiply(new THREE.Vector2(1, -1));
                    found = true;
                }

                if ((point.x == x1 && !toLeft) || (point.x == x2 && !toRight)) {
                    // Left/Right
                    vector = vector.multiply(new THREE.Vector2(-1, 1));
                    found = true;
                }
            }
            else if (type == "Cube") {
                let cube = intersected.object.userData.instance;

                let x1 = cube.x - 0.5;
                let x2 = x1 + 1;
                let y1 = cube.y - 0.5;
                let y2 = y1 + 1;

                let x = cube.boardX;
                let y = cube.boardY;

                let above = (this._blocks[y + 1] || [])[x];
                let below = (this._blocks[y - 1] || [])[x];
                let toLeft = this._blocks[y][x - 1];
                let toRight = this._blocks[y][x + 1];

                // Determine where each of the cubes intersected with the ball.
                let found = false;

                if ((point.y == y1 && !below) || (point.y == y2 && !above)) {
                    // Top/Bottom
                    vector = vector.multiply(new THREE.Vector2(1, -1));
                    found = true;
                }

                if ((point.x == x1 && !toLeft) || (point.x == x2 && !toRight)) {
                    // Left/Right
                    vector = vector.multiply(new THREE.Vector2(-1, 1));
                    found = true;
                }

                if (!found) {
                    console.log("hmm", point, x1, y1, x2, y2, above, below, toLeft, toRight);
                }
            }

            // Move ball to the point of intersection
            let point3d = new THREE.Vector3(point.x, point.y, 0);
            point3d.add(direction3d.clone().multiplyScalar(-this.ball.radius));
            this.ball.move(point3d.x, point3d.y);
        }

        this.ball.vector = vector;
    }

    /**
     * Drops the piece a single step.
     */
    lower() {
        // Bail if there is no piece on the board.
        if (!this._piece) {
            return;
        }

        // Subtract 1 unit from the Y coordinate
        this._piece.scene.position.y -= 1;
        this._pieceY--;

        if (this.pieceCollides()) {
            // Revert the lowering
            this._piece.scene.position.y += 1;
            this._pieceY++;

            // Land piece
            this.land();
        }
    }

    /**
     * Drops the piece as far down as it will go.
     */
    drop() {
        while(this.piece) {
            this.lower();
        }
    }

    /**
     * Rotates the current piece the given direction.
     */
    rotate(delta) {
        this._piece.rotate(delta);
    }

    /**
     * Cements the piece into the real board.
     */
    land() {
        this._piece.cubes.forEach( (point) => {
            let x = point.x + this._pieceX;
            let y = point.y + this._pieceY;

            let cube = new Cube({ color: this.piece.style });

            cube.mesh.position.x = x - 4.5;
            cube.mesh.position.y = y - 9.25;
            this._scene.add(cube.mesh);

            cube.boardX = x;
            cube.boardY = y;
            this._blocks[y][x] = cube;
        });

        this.piece = null;

        this.inspect();
    }

    /**
     * Checks for lines to clear.
     */
    inspect() {
        // Inspect from the top to the bottom
        for (var y = this._height - 1; y >= 0; y--) {
            let row = this._blocks[y];
            let filled = row.filter( x => x !== undefined );
            if (filled.length == this._width) {
                // Clear this row
                this.clear(y);
            }
        };
    }

    /**
     * Clears the given row and drops rows above.
     */
    clear(row) {
        this._blocks[row].forEach( (cube) => {
            if (cube) {
                this._scene.remove(cube.mesh);
            }
        });

        // Drop each above row
        // TODO: animate
        this._blocks.slice(row + 1).forEach( (row) => {
            row.forEach( (cube) => {
                if (cube) {
                    cube.mesh.position.y--;
                }
            });
        });

        this._blocks.splice(row, 1);
        this._blocks.push(new Array(this._width));
    }

    /**
     * Transitions to the block breaking phase.
     */
    transition() {
        this._boardRotateXTimestamp = 1;
    }

    /**
     * Performs game animation updates.
     *
     * @param {number} elapsed The time since the last update in milliseconds.
     */
    update(elapsed) {
        // If there is a non-zero value in the x-rotate timestamp, rotate!
        if (this._boardRotateXTimestamp) {
            this._boardRotateXTimestamp += elapsed;
            this._scene.rotation.x =
                -Math.PI * (Math.min(this._boardRotateXTimestamp, 1000) / 1000);

            if (this._boardRotateXTimestamp >= 1000) {
                this._boardRotateXTimestamp = 0;
            }
        }

        // Same for a non-zero value in the reverse x-rotate timestamp, rotate!
        if (this._boardRotateXReverseTimestamp) {
            this._boardRotateXReverseTimestamp += elapsed;
            this._scene.rotation.x =
                -Math.PI + Math.PI * (Math.min(
                    this._boardRotateXReverseTimestamp, 1000
                ) / 1000);

            if (this._boardRotateXReverseTimestamp >= 1000) {
                this._boardRotateXReverseTimestamp = 0;
            }
        }

        // Yep! If there is a non-zero value in the y-rotate timestamp, rotate!
        if (this._boardRotateYTimestamp) {
            this._boardRotateYTimestamp += elapsed;
            this._scene.rotation.y =
                2 * Math.PI * (Math.min(this._boardRotateYTimestamp, 1000) / 1000);

            if (this._boardRotateYTimestamp >= 1000) {
                this._boardRotateYTimestamp = 0;
                this._scene.rotation.y = 0;
            }
        }
    }
}
