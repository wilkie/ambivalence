"use strict";

import { Cube } from "./cube.js";

/**
 * This is a collection of Cube objects comprising a game piece.
 */
export class Piece {
    constructor(style) {
        // Create the cubes of the appropriate style
        this._cubes = Array.from({ length: 4 }, (_) => {
            return new Cube({ color: style });
        });

        this._style = style;
        this._properties = Piece.PIECES[style];

        // Add them to the scene group
        this._scene = new THREE.Scene();
        this._cubes.forEach( (cube) => {
            this._scene.add(cube.mesh);
        });

        // The current rotation
        this._rotation = 0;

        this.rotate(0);
    }

    /**
     * Retrieves the X position in the scene.
     */
    get x() {
        return this._scene.position.x;
    }

    /**
     * Retrieves the Y position in the scene.
     */
    get y() {
        return this._scene.position.y;
    }

    /**
     * Retrieves the object for the scene.
     */
    get scene() {
        return this._scene;
    }

    /**
     * Retrieves the block type.
     */
    get style() {
        return this._style;
    }

    /**
     * The positions of each cube.
     */
    get cubes() {
        return this._properties.rotations[this._rotation];
    }

    /**
     * Rotates the piece the given number of times in the given direction.
     */
    rotate(delta) {
        this._rotation += (delta + this._properties.rotations.length);
        this._rotation %= this._properties.rotations.length;

        // Move the cubes into formation
        this.cubes.forEach( (delta, i) => {
            this._cubes[i].mesh.position.x = delta.x;
            this._cubes[i].mesh.position.y = delta.y;
        });
    }
}

/**
 * The description of each of the game pieces.
 *
 * Every piece is described in a 4x4 cube space.
 *
 * The (x,y) pair describes where in that space each cube resides.
 *
 * The right-most set of numbers is the collision priority. This grid is only
 * 3 by 3 since I blocks cannot kick in ARS.
 *
 * See: https://tetris.wiki/Arika_Rotation_System
 *
 * 3 - X X X X    1 2 3  =  (x + 1 + ((3 - y) * 3))
 * 2 - X X X X    4 5 6
 * 1 - X X X X    7 8 9
 * 0 - X X X X
 *     | | | |
 *     0 1 2 3
 */
Piece.PIECES = [
    {
        rotations: [
            [
                { x:  0, y:  2 },   //  ....
                { x:  1, y:  2 },   //  ####
                { x:  2, y:  2 },   //  ....
                { x:  3, y:  2 },   //  ....
            ],
            [
                { x:  2, y:  0 },   //  ..#.
                { x:  2, y:  1 },   //  ..#.
                { x:  2, y:  2 },   //  ..#.
                { x:  2, y:  3 },   //  ..#.
            ]
        ]
    },
    {
        rotations: [
            [
                { x:  0, y:  2 },   //  ....
                { x:  1, y:  2 },   //  ##..
                { x:  1, y:  1 },   //  .##.
                { x:  2, y:  1 },   //  ....
            ],
            [
                { x:  2, y:  2 },   //  ..#.
                { x:  2, y:  3 },   //  .##.
                { x:  1, y:  2 },   //  .#..
                { x:  1, y:  1 },   //  ....
            ]
        ]
    },
    {
        rotations: [
            [
                { x:  0, y:  1 },   //  ....
                { x:  1, y:  2 },   //  .##.
                { x:  1, y:  1 },   //  ##..
                { x:  2, y:  2 },   //  ....
            ],
            [
                { x:  0, y:  2 },   //  #...
                { x:  0, y:  3 },   //  ##..
                { x:  1, y:  2 },   //  .#..
                { x:  1, y:  1 },   //  ....
            ]
        ]
    },
    {
        rotations: [
            [
                { x:  0, y:  2 },   //  ....
                { x:  0, y:  1 },   //  ###.
                { x:  1, y:  2 },   //  #...
                { x:  2, y:  2 },   //  ....
            ],
            [
                { x:  0, y:  3 },   //  ##..
                { x:  1, y:  3 },   //  .#..
                { x:  1, y:  2 },   //  .#..
                { x:  1, y:  1 },   //  ....
            ],
            [
                { x:  0, y:  1 },   //  ....
                { x:  1, y:  1 },   //  ..#.
                { x:  2, y:  1 },   //  ###.
                { x:  2, y:  2 },   //  ....
            ],
            [
                { x:  1, y:  1 },   //  .#..
                { x:  1, y:  2 },   //  .#..
                { x:  1, y:  3 },   //  .##.
                { x:  2, y:  1 },   //  ....
            ]
        ]
    },
    {
        rotations: [
            [
                { x:  0, y:  2 },   //  ....
                { x:  2, y:  1 },   //  ###.
                { x:  1, y:  2 },   //  ..#.
                { x:  2, y:  2 },   //  ....
            ],
            [
                { x:  0, y:  1 },   //  .#..
                { x:  1, y:  3 },   //  .#..
                { x:  1, y:  2 },   //  ##..
                { x:  1, y:  1 },   //  ....
            ],
            [
                { x:  0, y:  1 },   //  ....
                { x:  1, y:  1 },   //  #...
                { x:  2, y:  1 },   //  ###.
                { x:  0, y:  2 },   //  ....
            ],
            [
                { x:  1, y:  1 },   //  .##.
                { x:  1, y:  2 },   //  .#..
                { x:  1, y:  3 },   //  .#..
                { x:  2, y:  3 },   //  ....
            ]
        ]
    },
    {
        rotations: [
            [
                { x:  0, y:  2 },   //  ....
                { x:  1, y:  1 },   //  ###.
                { x:  1, y:  2 },   //  .#..
                { x:  2, y:  2 },   //  ....
            ],
            [
                { x:  0, y:  2 },   //  .#..
                { x:  1, y:  1 },   //  ##..
                { x:  1, y:  2 },   //  .#..
                { x:  1, y:  3 },   //  ....
            ],
            [
                { x:  0, y:  1 },   //  ....
                { x:  1, y:  1 },   //  .#..
                { x:  1, y:  2 },   //  ###.
                { x:  2, y:  1 },   //  ....
            ],
            [
                { x:  1, y:  1 },   //  .#..
                { x:  1, y:  2 },   //  .##.
                { x:  1, y:  3 },   //  .#..
                { x:  2, y:  2 },   //  ....
            ]
        ]
    },
    {
        rotations: [
            [
                { x:  1, y:  1 },   //  ....
                { x:  1, y:  2 },   //  .##.
                { x:  2, y:  1 },   //  .##.
                { x:  2, y:  2 },   //  ....
            ]
        ]
    },
];

Piece.PIECE_I = 0;
Piece.PIECE_Z = 1;
Piece.PIECE_S = 2;
Piece.PIECE_L = 3;
Piece.PIECE_J = 4;
Piece.PIECE_T = 5;
Piece.PIECE_O = 6;
