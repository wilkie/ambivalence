"use strict";

import { Board } from "./board.js";
import { Piece } from "./piece.js";
import { Ball } from "./ball.js";

/**
 * The game logic.
 */
export class Game {
    constructor(world) {
        this._world = world;

        this._board = new Board();

        this.newPiece();

        this._world.add(this._board.scene);

        // Timestamps
        this._timestamp = 0;
        this._dropTimestamp = 0;

        // Start in the block drop phase
        this._phase = 0;

        this._ball = new Ball();
        this._board.ball = this._ball;
    }

    /**
     * Performs the given game action.
     */
    perform(action) {
        if (action == "MoveLeft" || action == "MoveRight") {
            let delta = 1;
            if (action == "MoveLeft") {
                delta = -1;
            }

            this._board.move(delta);

            let index = this._board.pieceCollides();
            if (index) {
                this._board.move(-delta);
            }
        }
        else if (action == "RotateLeft" || action == "RotateRight") {
            let delta = 1;
            if (action == "RotateLeft") {
                delta = -1;
            }

            this._board.rotate(delta);

            let index = this._board.pieceCollides();
            if (index) {
                // I-piece never kicks
                if (this._board.piece.style == Piece.PIECE_I) {
                    this._board.rotate(-delta);
                }
                // L, J, T pieces do not kick on center intersect
                else if ((this._board.piece.style == Piece.PIECE_L ||
                          this._board.piece.style == Piece.PIECE_J ||
                          this._board.piece.style == Piece.PIECE_T) &&
                         (index == 2 || index == 5 || index == 8)) {
                    this._board.rotate(-delta);
                }
                else {
                    // Kick Right (maybe)
                    this._board.move(1);
                    if (this._board.pieceCollides()) {
                        // Kick Left (maybe)
                        this._board.move(-1);
                        this._board.move(-1);
                        if (this._board.pieceCollides()) {
                            this._board.move(1);
                            this._board.rotate(-delta);
                        }
                    }
                }
            }
        }
        else if (action == "MoveDown") {
            this._board.lower();
            this._dropTimestamp = 0;
        }
        else if (action == "Drop") {
            this._board.drop();
        }
    }

    /**
     * Updates the game each frame.
     *
     * @param {number} elapsed The time since the last update in milliseconds.
     */
    update(elapsed) {
        // Increment timestamps
        // The global timestamp
        this._timestamp += elapsed;

        // The timer for when a piece drops
        if (this._phase == 0) {
            this._dropTimestamp += elapsed;

            // If the board does not have an active piece, create one.
            if (!this._board.piece) {
                this.newPiece();
            }

            if (this._dropTimestamp > 1000) {
                //this._board.lower();
                this._dropTimestamp = 0;
            }
        }

        this._board.update(elapsed);
        this._ball.update(elapsed);
        if (this._board.ballCollides()) {
            console.log("heyo");
        }
    }

    /**
     * Generates a new game piece.
     */
    newPiece() {
        // Create the piece 'pool' which holds each piece.
        // This guarantees that we always see every piece regularly.
        if (this._piecePool === undefined || this._piecePool.length == 0) {
            this._piecePool = Game.shuffle([0, 1, 2, 3, 4, 5, 6]);
        }

        // Pull out the next random piece 'type'
        let type = this._piecePool.pop();

        // Create the piece.
        let piece = new Piece(type);

        // Throw it on the game board.
        this._board.piece = piece;
        this._dropTimestamp = 0;
    }

    /**
     * Shuffles the given array.
     *
     * Based on the shuffle found at (Daplie Labs, Apache 2.0):
     * https://github.com/Daplie/knuth-shuffle/blob/master/index.js
     */
    static shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }    
}
