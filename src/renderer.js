"use strict";

/**
 * Manages rendering the viewport.
 */
export class Renderer {
    constructor(canvas, game, world) {
        this._canvas = canvas;
        this._game = game;
        this._world = world;

        this._renderer = new THREE.WebGLRenderer( { canvas: this._canvas } );
        this._renderer.setSize(canvas.clientWidth, canvas.clientHeight);

        this.animate(0);
    }

    get canvas() {
        return this._canvas;
    }

    animate(timestamp) {
        if (this._lastTimestamp === undefined) {
            this._lastTimestamp = timestamp;
        }

        const elapsed = timestamp - this._lastTimestamp;
        this._lastTimestamp = timestamp;

        this._game.update(elapsed);
        this._renderer.render(this._world.scene, this._world.camera);

        window.requestAnimationFrame(this.animate.bind(this));
    }
}
