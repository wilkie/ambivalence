"use strict";

/**
 * This manages the interactive input for the game.
 */
export class Input {
    constructor(options = {}) {
        this.initializeKeyboard(options.bindings || {},
                                options.onAction);
    }

    initializeKeyboard(bindings, callback) {
        window.document.addEventListener("keydown", (event) => {
            var keyCode = event.code;

            Object.keys(bindings).forEach( (action) => {
                let codes = bindings[action];
                codes.forEach( (code) => {
                    if (keyCode == code) {
                        if (callback) {
                            callback(action);
                        }
                    }
                });
            });
        }, false);
    }
}
