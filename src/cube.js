"use strict";

export class Cube {
    constructor(options = {}) {
        let textures = Cube.textures();

        var geometry = new THREE.BoxGeometry();
        var material = new THREE.MeshBasicMaterial({ map: textures[options.color || 0] });
        this._cube = new THREE.Mesh(geometry, material);
        this._cube.userData = {
            instance: this,
            type: "Cube"
        };

        this._cube.layers.enable(1);

        this._boardX = -1;
        this._boardY = -1;
    }

    get x() {
        return this._cube.position.x;
    }

    get y() {
        return this._cube.position.y;
    }

    get mesh() {
        return this._cube;
    }

    get boardX() {
        return this._boardX;
    }

    set boardX(value) {
        this._boardX = value;
    }

    get boardY() {
        return this._boardY;
    }

    set boardY(value) {
        this._boardY = value;
    }

    static textures() {
        if (!Cube.__textures) {
            Cube.__textures = [
                new THREE.TextureLoader().load('textures/block01.png'),
                new THREE.TextureLoader().load('textures/block02.png'),
                new THREE.TextureLoader().load('textures/block03.png'),
                new THREE.TextureLoader().load('textures/block04.png'),
                new THREE.TextureLoader().load('textures/block05.png'),
                new THREE.TextureLoader().load('textures/block06.png'),
                new THREE.TextureLoader().load('textures/block07.png'),
                new THREE.TextureLoader().load('textures/block08.png'),
                new THREE.TextureLoader().load('textures/block09.png'),
                new THREE.TextureLoader().load('textures/block10.png')
            ];
        }

        return Cube.__textures;
    }
}
